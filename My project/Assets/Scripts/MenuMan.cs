using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuMan : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene("JuegoLibre");
    }

    public void Salir()
    {
        Application.Quit(); 
    }
}
