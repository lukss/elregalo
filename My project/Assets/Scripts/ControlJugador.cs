using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ControlJugador : MonoBehaviour
{
    public static float rapidezDesplazamiento = 9f;
    public Rigidbody rb;
    public float salto;
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 360.0f;
    private bool Piso = true;
    public int maxSaltos = 2;
    public int saltoActual = 0;
    public int velRot = 3;
    [SerializeField] float time = 1f;
    private float x, y;




    

    void Start()
    {
        rapidezDesplazamiento = 7f;
        rb = GetComponentInChildren<Rigidbody>();

    }


    

    void Update()
    {

        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Translate(x * Time.deltaTime * velocidadMovimiento, 0, y * Time.deltaTime * velocidadMovimiento);

        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);


        if (Input.GetButtonDown("Jump") && (Piso || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, salto, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * salto, ForceMode.Impulse);
            Piso = false;
            saltoActual++;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }




    }

 

}
