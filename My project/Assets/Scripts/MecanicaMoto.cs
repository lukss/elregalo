using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MecanicaMoto : MonoBehaviour
{
    public float speed = 10f;
    public float turnSpeed = 5f;
    public Transform Rueda1;
    public Transform Rueda2;

    private Rigidbody rb;
    private float verticalInput,horizontalInput;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }

    void Update()
    {
        HandleMotor();
        HandleSteering();
        RotarRuedas();

    }

    void HandleMotor()
    {
         verticalInput = Input.GetAxis("Vertical");
        rb.AddForce(transform.forward * verticalInput * speed, ForceMode.Acceleration);

    }

    void HandleSteering()
    {
         horizontalInput = Input.GetAxis("Horizontal");
        Vector3 turn = new Vector3(0, horizontalInput * turnSpeed * Time.deltaTime, 0);
        rb.MoveRotation(rb.rotation * Quaternion.Euler(turn));

        Rueda1.localRotation = Quaternion.Euler(0, horizontalInput * 30f, 0);
    }
    void RotarRuedas()
    {
       Rueda1.Rotate(Vector3.right * rb.velocity.magnitude * Time.deltaTime * 10f);
       Rueda2.Rotate(Vector3.right * rb.velocity.magnitude * Time.deltaTime * 10f);
    }
}
